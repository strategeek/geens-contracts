pragma solidity ^0.4.11;

/*
	Contract determines the owner of geecoins
*/
contract Ownable {
    address public owner;

    function Ownable() {
        owner = msg.sender;
    }
	
	function transferOwnership (address newOwner) {
		if (newOwner != address(0)) {
			owner = newOwner;
		}
	}
    
    modifier onlyOwner { //only owner can call function
        require (msg.sender == owner);
        _;
    }
}
contract Trustable is Ownable {

	mapping (address => bool) trusted; //addresses which are trusted -> able to call transfer on crowdsale

    function Trustable()  {
        trusted[msg.sender] = true;
    }
    
	function addTrusted(address _address) onlyOwner { //add new trusted address
		if (_address != address(0)){ //if not 0x0 address
        	trusted[_address] = true;
		}
    }

    function removeTrusted(address _address) onlyOwner {
		if (_address != address(0)){ //if not 0x0 address
        	trusted[_address] = false;
		}
    }
}

/*
	Safe math functions
*/
library SafeMath {
	/*
		@return sum of a and b
	*/
	function safeAdd (uint a, uint b) internal returns (uint) {
		uint c = a + b;
		assert(c >= a);
		return c;
	}
	/*
		@return difference of a and b
	*/
	function safeSub (uint a, uint b) internal returns (uint) {
		assert(a >= b);
		return a - b;
	}
	/*
		@return multiples a and b
	*/
	
	function safeMul (uint a, uint b) internal returns (uint) {
		uint c = a * b;
		assert (a == 0 || c / a == b);
		return c;
	}
	/*
		@return a / b
	*/
	function safeDiv (uint a, uint b)  internal returns (uint) {
	   // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint c = a / b;
      //  assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
	}
}
/*
	Crowdsale contarct interface
*/
contract Crowdsale {
	function isCrowdsale () returns (bool);
}
/*
	Contract determines token

*/
contract Token is Trustable {
    
    uint8 public decimals; //number of decimals in geecoins
    uint256 public totalSupply; //total amount of geecoins
    string public name; //name of token
    string public symbol; //symbol of token
	Crowdsale ico;

    
    mapping (address => uint256) public balanceOf; //stores token balances by address

    event Transfer(address indexed from, address indexed to, uint256 value) ; //to keep track of transferings

    /*
		Token transfering function
	*/
    function transfer (address to, uint value) balanceChanges(to, value)  notZeroAddress(to) {
		if (ico.isCrowdsale()) {
	        require (trusted[msg.sender]); //require the end of funding or msg.sender to be trusted address
		}
        balanceOf[msg.sender] = SafeMath.safeSub(balanceOf[msg.sender], value);                     
        balanceOf[to] = SafeMath.safeAdd(balanceOf[to], value);                     
        Transfer(msg.sender, to, value);  
    }
	
	function setCrowdsale (Crowdsale _address) notZeroAddress(_address) onlyOwner {
		ico = _address;
	}
	
    modifier notZeroAddress (address add) { //cannot interact with zero address
        require (add != address(0));                               
        _;
    }
    
    modifier balanceChanges (address to, uint value){ //check if balance changes
       require (balanceOf[to] + value > balanceOf[to]) ; 
       _;
    }
}


/*
	Inspired by Civic and Golem

*/
/*
	Interface of megrate agent contract (the new token contract)
*/

contract MigrateAgent {
	
  function migrateFrom(address _tokenHolder, uint256 _amount) external;
	
}


contract Migratable is Token{

	MigrateAgent migrateAgent;
	uint totalMigrated; //total migrated tokens

  /**
   * Migrate states.
   *
   * - NotAllowed: The child contract has not reached a condition where the upgrade can bgun
   * - WaitingForAgent: Token allows upgrade, but we don't have a new agent yet
   * - ReadyToMigrate: The agent is set, but not a single token has been upgraded yet
   * - Migrating: Upgrade agent is set and the balance holders can upgrade their tokens
   *
   */
	
	enum MigrateState {Unknown, NotAllowed, WaitingForAgent, ReadyToMigrate, Migrating}
	
	event Migrate (address indexed from, address indexed to, uint value);
	
	event MigrateAgentSet(address agent);
	
	function migrate (uint value) {
	
	 	MigrateState state = getMigrateState();
     	require(state == MigrateState.ReadyToMigrate || state == MigrateState.Migrating); //migrating has started
        require (value > 0); 
		
		//migrates from this contract
      	balanceOf[msg.sender] = SafeMath.safeSub(balanceOf[msg.sender], value); //migrates user balance
      	totalSupply = SafeMath.safeSub(totalSupply, value); //migrates total supply

		totalMigrated = SafeMath.safeAdd(totalMigrated, value); //counts migrated tokens

      	// Upgrade agent reissues the tokens
      	migrateAgent.migrateFrom(msg.sender, value);
      	Migrate(msg.sender, migrateAgent, value);
	}
	
	
	/*
		set migrating agent and start migrating
	*/
  function setMigrateAgent(MigrateAgent agent) external onlyOwner {
    
      require(!ico.isCrowdsale()); //can migrate just after funding
	  require (agent != address(0)); //not 0x0 address
      require (getMigrateState() == MigrateState.Migrating); //cannot interupt migrating
      migrateAgent = agent; //set migrate agent
      MigrateAgentSet(migrateAgent); //event
  }
  
  /*
  	migrating status
  */
  function getMigrateState() public constant returns(MigrateState) {
    if(ico.isCrowdsale()) return MigrateState.NotAllowed; //migratation not allowed on funding
    else if(address(migrateAgent) == address(0)) return MigrateState.WaitingForAgent; //migrateing address is not set
    else if(totalMigrated == 0) return MigrateState.ReadyToMigrate; //migrating hasn't started
    else return MigrateState.Migrating; //migrating
  }
	
}

/*
	Contract defines specific token
*/
contract GeeToken is Migratable{
    
    uint8 public decimals; //number of decimals in geecoins
    string public name; //name of token
    string public symbol; //symbol of token

    function GeeToken( //constructor: need to write this values on contract creation
        uint256 initialSupply, //initial supply of token
        string tokenName, //token name
        uint8 decimalUnits, //number of decimals
        string tokenSymbol //token symbol
        ) {
        balanceOf[msg.sender] = initialSupply;   //msg.sender gets all tokens at the beginning           
        totalSupply = initialSupply;                        
        name = tokenName;                                   
        symbol = tokenSymbol;                               
        decimals = decimalUnits;   
    }
}


