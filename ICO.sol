pragma solidity ^0.4.11;

/*
	Contract determines the owner of geecoins
*/
contract Ownable {
    address public owner;

    function Ownable() {
        owner = msg.sender;
    }
	
	function transferOwnership (address newOwner) {
		if (newOwner != address(0)) {
			owner = newOwner;
		}
	}
    
    modifier onlyOwner { //only owner can call function
        require (msg.sender == owner);
        _;
    }
}

/*
	Safe math functions
*/
library SafeMath {
	/*
		@return sum of a and b
	*/
	function safeAdd (uint a, uint b) internal returns (uint) {
		uint c = a + b;
		assert(c >= a);
		return c;
	}
	/*
		@return difference of a and b
	*/
	function safeSub (uint a, uint b) internal returns (uint) {
		assert(a >= b);
		return a - b;
	}
	/*
		@return multiples a and b
	*/
	
	function safeMul (uint a, uint b) internal returns (uint) {
		uint c = a * b;
		assert (a == 0 || c / a == b);
		return c;
	}
	/*
		@return a / b
	*/
	function safeDiv (uint a, uint b)  internal returns (uint) {
	   // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint c = a / b;
      //  assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
	}
}
/*
	Interface of Geecoins contract
*/
contract GeeToken {
    function transfer (address to, uint amount);
}

contract Crowdsale is Ownable {

    uint public sold; //counts how many geecoins is sold
    uint public constant hardCap = 6700000000000000; //hard cap in geecoins with 8 decimals
    uint public price = 3000000; //price at the beginning
    uint public constant startBlockNumber = 1592416; //Sep-01-2017 12:00:07 PM +UTC
    uint public endBlockNumber = startBlockNumber + 152460; // start + 30 days
    uint public tier2 = startBlockNumber + 15246 ; //start + 3 days
    uint public tier3 = startBlockNumber + 50820; //start + 10 days ( 3 days + 7 days)
    uint public tier4 = startBlockNumber + 101640; //start + 20 days ( 3 days + 7 days + 10 days)
    uint public constant tier1Price = 3000000; //price in 1st tier
    uint public constant tier2Price = 3300000; //price in 2nd tier
    uint public constant tier3Price = 3700000; //price in 3rd tier
    uint public constant tier4Price = 4100000; //price in 4th tier
	bool public paused = false; //to check is ico is paused
	uint public pauseBlockNumber; //block number on pause
    GeeToken gee; //geecoin contract address
    
    
    event Buy (address indexed who, uint256 amount); //keep track of buyings
    
    function Crowdsale (GeeToken geeAddress) payable { //payable - can store ETH 
        gee = geeAddress;
    }
    
    function () payable { //fallback function - is called when ether is sent to contract
		require(!paused);
        require (msg.value >= 0.03 ether && msg.value < 1000 ether); //ether limitation
        require (startBlockNumber <= block.number); //is crowdsale began
        require (endBlockNumber >= block.number); //is crowdsale ended
        if (price != tier4Price)  priceChanger(); //if last tier hasn't started, change price if needed
        uint256 amount = SafeMath.safeDiv (msg.value, price);    //counts how many GEE sender can buy
		require (SafeMath.safeAdd (sold, amount) <= hardCap);
        sold = SafeMath.safeAdd (sold, amount); //adds amount to sold
		if (sold == hardCap) {
			endBlockNumber = block.number;
			finalize();
		}
        gee.transfer(msg.sender, amount); //transfers amount of geecoins to msg.sender       
        owner.transfer(this.balance);  //transfer contract ethers to geens
        Buy (msg.sender, amount);  

    }
	/*
		To pause crowdsale if something wrong happens
	*/
	function pause () onlyOwner {
		require(!paused);
		paused = true;
		pauseBlockNumber = block.number;
		
	}
	/*
		To restart crowdsale after pause
	*/
	function restart() onlyOwner {
		require (paused);
		paused = false;
		uint blocks = SafeMath.safeSub(block.number, pauseBlockNumber);
		tier2 = SafeMath.safeAdd(tier2, blocks);
		tier3 = SafeMath.safeAdd(tier3, blocks);
		tier4 = SafeMath.safeAdd(tier4, blocks);
		endBlockNumber = SafeMath.safeAdd(endBlockNumber, blocks);
	}
    /*
        function which automaticly changes tiers
    */
    function priceChanger() internal {
        if (block.number >= tier4) {
            price = tier4Price;
        } else if (block.number >= tier3){
            price = tier3Price;
        } else if (block.number >= tier2){
            price = tier2Price;
        }
    }
	
	/*
		get not sold geecoins when crowdsale ended
	*/
	function finalize () {
		if (block.number > endBlockNumber && sold != hardCap) {
			gee.transfer(owner, SafeMath.safeSub(hardCap, sold));
		}
	}
	
	/*
		function returns is Crowdsale is open
	*/
	
	function isCrowdsale () returns (bool) {
	    uint blocks = SafeMath.safeSub(block.number, pauseBlockNumber);
		if (SafeMath.safeAdd(endBlockNumber, blocks ) < block.number) return false;
		else return true;
	}
}